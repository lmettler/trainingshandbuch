package services;

import models.Handbuch;
import models.User;
import models.Handbuch_User;
import models.Training;
import repository.HandbuchRepository;

import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;
import javax.inject.Inject;

public class DefaultHandbuchService implements HandbuchService {

    private HandbuchRepository handbuchRepository;

    @Inject
    public DefaultHandbuchService(HandbuchRepository handbuchRepository) {
        this.handbuchRepository = handbuchRepository;
    }

    public CompletionStage<Stream<Handbuch>> get() {
        return handbuchRepository.list();
    }

    public CompletionStage<Stream<Handbuch>> getHandbuchUser(final Long id) {
        return handbuchRepository.findHandbuchUser(id);
    }

    public CompletionStage<Handbuch> getHandbuch(final Long id) {
        return handbuchRepository.findHandbuch(id);
    }

    public CompletionStage<Boolean> delete(final Long id) {
        return handbuchRepository.remove(id);
    }

    // public CompletionStage<Handbuch> update(final Handbuch updatedHandbuch) {
    //     return handbuchRepository.update(updatedHandbuch);
    // }

    public CompletionStage<Handbuch> addHandbuch(final Handbuch handbuch) {
        return handbuchRepository.addhandbuch(handbuch);
    }

    public CompletionStage<Handbuch_User> addHandbuchUser(final Handbuch_User handbuchuser) {
        return handbuchRepository.addhandbuchuser(handbuchuser);
    }

    public CompletionStage<Stream<Training>> getTraining(final Long handbuchId) {
        return handbuchRepository.listtraining(handbuchId);
    }
}

