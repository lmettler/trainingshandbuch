package services;

import models.Training;
import models.Disziplin;
import models.Disziplin_Training;
import models.Schwerpunkt;
import repository.TrainingRepository;

import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;
import javax.inject.Inject;

public class DefaultTrainingService implements TrainingService {

    private TrainingRepository trainingRepository;

    @Inject
    public DefaultTrainingService(TrainingRepository trainingRepository) {
        this.trainingRepository = trainingRepository;
    }

    public CompletionStage<Stream<Training>> get() {
        return trainingRepository.listtraining();
    }

    public CompletionStage<Training> get(final Long id) {
        return trainingRepository.find(id);
    }

    public CompletionStage<Training> addTraining(final Training training) {
        return trainingRepository.addtraining(training);
    }

    public CompletionStage<Boolean> delete(final Long id) {
        return trainingRepository.remove(id);
    }

    public CompletionStage<Training> update(final Training updatedTraining) {
        return trainingRepository.update(updatedTraining);
    }

    public CompletionStage<Disziplin> addDisziplin(final Disziplin disziplin) {
        return trainingRepository.adddisziplin(disziplin);
    }

    public CompletionStage<Disziplin_Training> addDisziplinTraining(final Disziplin_Training disziplintraining) {
        return trainingRepository.adddisziplintraining(disziplintraining);
    }

    public CompletionStage<Stream<Schwerpunkt>> getSchwerpunkt() {
        return trainingRepository.listschwerpunkt();
    }

    public CompletionStage<Stream<Disziplin>> getAllDisziplin() {
        return trainingRepository.listalldisziplin();
    }

    public CompletionStage<Stream<Disziplin>> getDisziplin(Long schwerpunktId) {
        return trainingRepository.listdisziplin(schwerpunktId);
    }

    public CompletionStage<Stream<Disziplin>> getDisziplinen(final Long trainingId) {
        return trainingRepository.listdisziplinen(trainingId);
    }

    public CompletionStage<Boolean> deleteTrainingDisziplin(final Disziplin_Training td) {
        return trainingRepository.removetrainingdisziplin(td);
    }
}

