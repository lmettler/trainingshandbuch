package services;

import com.google.inject.ImplementedBy;
import models.Handbuch;
import models.User;
import models.Handbuch_User;
import models.Training;

import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;

@ImplementedBy(DefaultHandbuchService.class)

public interface HandbuchService {

    /**
     * Returns List of all Handbuecher
     *
     * @return list of all Handbuecher
     */
    CompletionStage<Stream<Handbuch>> get();

    /**
     * Returns Card Index with given identifier.
     *
     * @param id user identifier
     * @return handbuch with given userId
     */
    CompletionStage<Stream<Handbuch>> getHandbuchUser(final Long id);

    /**
     * Returns Card Index with given identifier.
     *
     * @param id handbuch identifier
     * @return handbuch with given identifier or {@code null}
     */
    CompletionStage<Handbuch> getHandbuch(final Long id);

    /**
     * Removes Handbuch with given identifier.
     *
     * @param id handbuch identifier
     * @return {@code true} on success  {@code false} on failure
     */
    CompletionStage<Boolean> delete(final Long id);

    /**
     * Updates Card Index with given identifier.
     * @param updatedHandbuch book with updated fields
     * @return updated CardIndex
     */
    // CompletionStage<Handbuch> update(final Handbuch updatedHandbuch);

    /**
     * Adds the given Card Index.
     *
     * @param handbuch to add
     * @return added book
     */
    CompletionStage<Handbuch> addHandbuch(final Handbuch handbuch);

    /**
     * Adds the given Handbuch and User Inex.
     *
     * @param HandbuchUser to add
     * @return added HandbuchUser
     */
    CompletionStage<Handbuch_User> addHandbuchUser(final Handbuch_User handbuchuser);

    /**
     * Returns Card Index with given identifier.
     *
     * @param id handbuch identifier
     * @return trainings with given identifier or {@code null}
     */
    CompletionStage<Stream<Training>> getTraining(final Long handbuchId);
}
