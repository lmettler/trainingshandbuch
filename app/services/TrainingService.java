package services;

import com.google.inject.ImplementedBy;
import models.Training;
import models.Disziplin;
import models.Disziplin_Training;
import models.Schwerpunkt;

import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;

@ImplementedBy(DefaultTrainingService.class)

public interface TrainingService {

    /**
     * Returns List of all Handbuecher
     *
     * @return list of all Handbuecher
     */
    CompletionStage<Stream<Training>> get();

    /**
     * Returns Card Index with given identifier.
     *
     * @param id training identifier
     * @return training with given identifier or {@code null}
     */
    CompletionStage<Training> get(final Long id);

    /**
     * Removes Handbuch with given identifier.
     *
     * @param id handbuch identifier
     * @return {@code true} on success  {@code false} on failure
     */
    CompletionStage<Boolean> delete(final Long id);

    /**
     * Updates Card Index with given identifier.
     *
     * @param updatedHandbuch book with updated fields
     * @return updated CardIndex
     */
    CompletionStage<Training> update(final Training updatedTraining);

    /**
     * Adds the given Card Index.
     *
     * @param training to add
     * @return added training
     */
    CompletionStage<Training> addTraining(final Training training);

    /**
     * Adds the given User Index.
     *
     * @param user to add
     * @return added user
     */
    CompletionStage<Disziplin> addDisziplin(final Disziplin disziplin);

    /**
     * Adds the given Handbuch and User Inex.
     *
     * @param HandbuchUser to add
     * @return added HandbuchUser
     */
    CompletionStage<Disziplin_Training> addDisziplinTraining(final Disziplin_Training disziplintraining);

    /**
     * Returns all Schwerpunkte
     *
     * @return Schwerpunkte
     */
    CompletionStage<Stream<Schwerpunkt>> getSchwerpunkt();

    /**
     * Returns all Disziplinen
     *
     * @return Disziplinen
     */
    CompletionStage<Stream<Disziplin>> getAllDisziplin();

    /**
     * Returns all Disziplinen
     *
     * @param schwerpunktId schwerpunkt identifier
     * @return Disziplinen
     */
    CompletionStage<Stream<Disziplin>> getDisziplin(final Long schwerpunktId);

    /**
     * Returns all Disziplinen eines Trainings
     *
     * @param trainingId training identifier
     * @return Disziplinen
     */
    CompletionStage<Stream<Disziplin>> getDisziplinen(final Long trainingId);

    /**
     * Removes Disziplin Handbuch Relation with given identifier.
     *
     * @param td Disziplin_Training identifier
     * @return {@code true} on success  {@code false} on failure
     */
    CompletionStage<Boolean> deleteTrainingDisziplin(final Disziplin_Training td);
}
