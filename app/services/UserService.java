package services;

import com.google.inject.ImplementedBy;
import models.Handbuch;
import models.User;
import models.Handbuch_User;
import models.Training;

import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;

@ImplementedBy(DefaultUserService.class)

public interface UserService {

    /**
     * Adds the given User Inex.
     *
     * @param user to add
     * @return added user
     */
    CompletionStage<User> addUser(final User user);

    /**
     * Adds the given User Inex.
     *
     * @param user to get
     * @return get user
     */
    CompletionStage<User> getUser(final User user);
}
