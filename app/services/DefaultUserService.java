package services;

import models.Handbuch;
import models.User;
import models.Handbuch_User;
import models.Training;
import repository.UserRepository;

import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;
import javax.inject.Inject;

public class DefaultUserService implements UserService {

    private UserRepository userRepository;

    @Inject
    public DefaultUserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public CompletionStage<User> addUser(final User user) {
        return userRepository.adduser(user);
    }

    public CompletionStage<User> getUser(final User user) {

        return userRepository.getuser(user);
    }
}