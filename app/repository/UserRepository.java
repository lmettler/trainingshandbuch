package repository;

import models.Handbuch;
import models.User;
import models.Handbuch_User;
import models.Training;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.function.Function;
import java.util.stream.Stream;

import static java.util.concurrent.CompletableFuture.supplyAsync;

public class UserRepository {

    private final JPAApi jpaApi;

    @Inject
    public UserRepository(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }


    public CompletionStage<User> adduser(User user) {
        return supplyAsync(() -> wrap(em -> insert(em, user)));
    }

    public CompletionStage<User> getuser(User user) {
        return supplyAsync(() -> wrap(em -> getuser(em, user)));
    }

    private <T> T wrap(Function<EntityManager, T> function) {
        return jpaApi.withTransaction(function);
    }

    private User getuser(EntityManager em, User user) {
        User userexist = em.createQuery("select c from tUser c where c.email = '" + user.getEmail() + "'", User.class).getSingleResult();
        return userexist;
    }

    private User insert(EntityManager em, User user) {
        em.persist(user);
        return user;
    }
}