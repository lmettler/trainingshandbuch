package repository;

import models.Training;
import models.Disziplin;
import models.Disziplin_Training;
import models.Schwerpunkt;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.persistence.EntityManager;


import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.function.Function;
import java.util.stream.Stream;

import static java.util.concurrent.CompletableFuture.supplyAsync;

public class TrainingRepository {

    private final JPAApi jpaApi;

    @Inject
    public TrainingRepository(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    public CompletionStage<Training> addtraining(Training training) {
        return supplyAsync(() -> wrap(em -> insert(em, training)));
    }

    public CompletionStage<Disziplin> adddisziplin(Disziplin disziplin) {
        return supplyAsync(() -> wrap(em -> insert(em, disziplin)));
    }

    public CompletionStage<Disziplin_Training> adddisziplintraining(Disziplin_Training dt) {
        return supplyAsync(() -> wrap(em -> insert(em, dt)));
    }

    public CompletionStage<Training> update(Training training) {
        return supplyAsync(() -> wrap(em -> update(em, training)));
    }

    public CompletionStage<Training> find(Long id) {
        return supplyAsync(() -> wrap(em -> find(em, id)));
    }

    public CompletionStage<Boolean> remove(Long id) {
        return supplyAsync(() -> wrap(em -> remove(em, id)));
    }

    public CompletionStage<Stream<Training>> listtraining() {
        return supplyAsync(() -> wrap(this::listtraining));
    }

    public CompletionStage<Stream<Schwerpunkt>> listschwerpunkt() {
        return supplyAsync(() -> wrap(this::listschwerpunkt));
    }

    public CompletionStage<Stream<Disziplin>> listalldisziplin() {
        return supplyAsync(() -> wrap(this::listalldisziplin));
    }


    public CompletionStage<Stream<Disziplin>> listdisziplin(Long schwerpunktId) {
        return supplyAsync(() -> wrap(em -> listdisziplin(em, schwerpunktId)));
    }

    public CompletionStage<Stream<Disziplin>> listdisziplinen(Long trainingId) {
        return supplyAsync(() -> wrap(em -> listdisziplinen(em, trainingId)));
    }

    public CompletionStage<Boolean> removetrainingdisziplin(Disziplin_Training dt) {
        return supplyAsync(() -> wrap(em -> removetrainingdisziplin(em, dt)));
    }

    private <T> T wrap(Function<EntityManager, T> function) {
        return jpaApi.withTransaction(function);
    }

    private Training insert(EntityManager em, Training training) {
        em.persist(training);
        return training;
    }

    private Disziplin insert(EntityManager em, Disziplin disziplin) {
        em.persist(disziplin);
        return disziplin;
    }

    private Disziplin_Training insert(EntityManager em, Disziplin_Training dt) {
        List<Disziplin_Training> exists = em.createQuery("select c from disziplin_training c where c.disziplinId = " + dt.getDisziplinId() +
                " AND c.trainingId = " + dt.getTrainingId(), Disziplin_Training.class).getResultList();
        if (exists.isEmpty()) {
            em.persist(dt);
        }
        ;
        return dt;
    }

    private Training update(EntityManager em, Training training) {
        Training updatedTraining = em.find(Training.class, training.getTrainingId());
        updatedTraining.setHandbuchId(training.getHandbuchId());
        updatedTraining.setTrainingDatum(training.getTrainingDatum());
        updatedTraining.setTrainingArt(training.getTrainingArt());
        updatedTraining.setEinleitung(training.getEinleitung());
        updatedTraining.setHauptteil(training.getHauptteil());
        updatedTraining.setSchluss(training.getSchluss());
        updatedTraining.setBemerkungen(training.getBemerkungen());
        return updatedTraining;
    }

    private Training find(EntityManager em, Long id) {
        return em.find(Training.class, id);
    }


    private Boolean remove(EntityManager em, Long id) {
        Training removedTraining = em.find(Training.class, id);
        if (null != removedTraining) {
            em.remove(removedTraining);
            return true;
        }
        return false;
    }

    private Stream<Training> listtraining(EntityManager em) {
        List<Training> training = em.createQuery("select c from Training c", Training.class).getResultList();
        return training.stream();
    }

    private Stream<Schwerpunkt> listschwerpunkt(EntityManager em) {
        List<Schwerpunkt> schwerpunkt = em.createQuery("select c from Schwerpunkt c", Schwerpunkt.class).getResultList();
        return schwerpunkt.stream();
    }

    private Stream<Disziplin> listalldisziplin(EntityManager em) {
        List<Disziplin> disziplins = em.createQuery("select c from Disziplin c", Disziplin.class).getResultList();
        return disziplins.stream();
    }

    private Stream<Disziplin> listdisziplin(EntityManager em, Long id) {
        List<Disziplin> disziplin = em.createQuery("select c from Disziplin c WHERE schwerpunktid = " + id, Disziplin.class).getResultList();
        return disziplin.stream();
    }

    private Stream<Disziplin> listdisziplinen(EntityManager em, Long id) {
        List<Disziplin> disziplin = em.createQuery("select c from Disziplin c, disziplin_training dt " +
                "WHERE c.disziplinId = dt.disziplinId AND dt.trainingId = " + id, Disziplin.class).getResultList();
        return disziplin.stream();
    }

    private Boolean removetrainingdisziplin(EntityManager em, Disziplin_Training dt) {
        Disziplin_Training removedDisziplin = em.createQuery("select c from disziplin_training c where c.disziplinId = " + dt.getDisziplinId() +
                " AND c.trainingId = " + dt.getTrainingId(), Disziplin_Training.class).getSingleResult();
        if (null != removedDisziplin) {
            em.remove(removedDisziplin);
            return true;
        }
        return false;
    }
}
