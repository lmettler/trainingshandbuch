package repository;

import models.Handbuch;
import models.User;
import models.Handbuch_User;
import models.Training;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.function.Function;
import java.util.stream.Stream;

import static java.util.concurrent.CompletableFuture.supplyAsync;

public class HandbuchRepository {

    private final JPAApi jpaApi;

    @Inject
    public HandbuchRepository(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    public CompletionStage<Handbuch> addhandbuch(Handbuch handbuch) {
        return supplyAsync(() -> wrap(em -> insert(em, handbuch)));
    }

    public CompletionStage<Handbuch_User> addhandbuchuser(Handbuch_User hu) {
        return supplyAsync(() -> wrap(em -> insert(em, hu)));
    }

    // public CompletionStage<Handbuch> update(Handbuch handbuch) {
    //     return supplyAsync(() -> wrap(em -> update(em, handbuch)));
    // }

    public CompletionStage<Stream<Handbuch>> findHandbuchUser(Long id) {
        return supplyAsync(() -> wrap(em -> findHandbuchUser(em, id)));
    }

    public CompletionStage<Handbuch> findHandbuch(Long id) {
        return supplyAsync(() -> wrap(em -> findHandbuch(em, id)));
    }

    public CompletionStage<Stream<Training>> listtraining(Long handbuchId) {
        return supplyAsync(() -> wrap(em -> listtraining(em, handbuchId)));
    }

    public CompletionStage<Boolean> remove(Long id) {
        return supplyAsync(() -> wrap(em -> remove(em, id)));
    }

    public CompletionStage<Stream<Handbuch>> list() {
        return supplyAsync(() -> wrap(this::list));
    }

    private <T> T wrap(Function<EntityManager, T> function) {
        return jpaApi.withTransaction(function);
    }

    private Handbuch insert(EntityManager em, Handbuch handbuch) {
        em.persist(handbuch);
        return handbuch;
    }

    private Handbuch_User insert(EntityManager em, Handbuch_User hu) {
        em.persist(hu);
        return hu;
    }

    // private Handbuch update(EntityManager em, Handbuch handbuch) {
    //     Handbuch updatedHandbuch = em.find(Handbuch.class, handbuch.getHandbuchId());
    //     updatedHandbuch.setGruppenname(handbuch.getGruppenname());
    //     updatedHandbuch.setStartdatum(handbuch.getStartdatum());
    //     updatedHandbuch.setEnddatum(handbuch.getEnddatum());
    //     updatedHandbuch.setTrainingstag(handbuch.getTrainingstag());
    //     updatedHandbuch.setTurnverein(handbuch.getTurnverein());
    //     return updatedHandbuch;
    // }

    private Stream<Handbuch> findHandbuchUser(EntityManager em, Long id) {

        List<Handbuch> handbuch = em.createQuery("select c from Handbuch c, handbuch_user hu " +
                "WHERE c.handbuchId = hu.handbuchId AND hu.userId = " + id, Handbuch.class).getResultList();
        return handbuch.stream();
    }

    private Handbuch findHandbuch(EntityManager em, Long id) {

        Handbuch handbuch = em.find(Handbuch.class, id);
        return handbuch;
    }

    private Boolean remove(EntityManager em, Long id) {
        Handbuch removedHandbuch = em.find(Handbuch.class, id);
        if (null != removedHandbuch) {
            em.remove(removedHandbuch);
            return true;
        }
        return false;
    }

    private Stream<Handbuch> list(EntityManager em) {
        List<Handbuch> handbuch = em.createQuery("select c from Handbuch c", Handbuch.class).getResultList();
        return handbuch.stream();
    }

    private Stream<Training> listtraining(EntityManager em, Long id) {
        List<Training> training = em.createQuery("select c from Training c WHERE handbuchid = " + id, Training.class).getResultList();
        return training.stream();
    }
}
