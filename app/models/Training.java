package models;

import javax.persistence.*;
import javax.persistence.Id;

import java.util.Date;

@Entity(name = "Training")
public class Training {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long trainingId;
    private long handbuchId;
    @Basic
    @Temporal(TemporalType.DATE)
    private Date trainingDatum;
    private String trainingArt;
    private String einleitung;
    private String hauptteil;
    private String schluss;
    private String bemerkungen;

    public long getTrainingId() {
        return trainingId;
    }

    public void setTrainingId(long trainingId) {
        this.trainingId = trainingId;
    }

    public long getHandbuchId() {
        return handbuchId;
    }

    public void setHandbuchId(long handbuchId) {
        this.handbuchId = handbuchId;
    }
    
    public Date getTrainingDatum() {
        return trainingDatum;
    }

    public void setTrainingDatum(Date trainingDatum) {
        this.trainingDatum = trainingDatum;
    }

    public String getTrainingArt() {
        return trainingArt;
    }

    public void setTrainingArt(String trainingArt) {
        this.trainingArt = trainingArt;
    }

    public String getEinleitung() {
        return einleitung;
    }

    public void setEinleitung(String einleitung) {
        this.einleitung = einleitung;
    }

    public String getHauptteil() {
        return hauptteil;
    }

    public void setHauptteil(String hauptteil) {
        this.hauptteil = hauptteil;
    }

    public String getSchluss() {
        return schluss;
    }

    public void setSchluss(String schluss) {
        this.schluss = schluss;
    }

    public String getBemerkungen() {
        return bemerkungen;
    }

    public void setBemerkungen(String bemerkungen) {
        this.bemerkungen = bemerkungen;
    }
}

