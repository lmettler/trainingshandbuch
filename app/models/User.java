package models;

import javax.persistence.*;
import javax.persistence.JoinTable;
import java.util.Set;

@Entity(name = "tUser")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long userId;
    private String vorname;
    private String nachname;
    private String passwort;
    private String email;
    private String turnverein;
    //
    // private String rolle; used in future

    // @ManyToMany
    // @JoinTable
    // (name="handbuch_user",
    //     joinColumns={@JoinColumn(name="userId", referencedColumnName="userId")},
    //     inverseJoinColumns={@JoinColumn(name="handbuchId", referencedColumnName="handbuchId")})
    // private Set<Handbuch> handbuch;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public String getPasswort() {

        return passwort;
    }

    public void setPasswort(String passwort) {

        this.passwort = passwort;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTurnverein() {
        return turnverein;
    }

    public void setTurnverein(String turnverein) {
        this.turnverein = turnverein;
    }

    // public Set<Handbuch> getUserhandbuch() {
    //     return handbuch;
    // }

    // public void setUserhandbuch(Set<Handbuch> handbuch) {
    //     this.handbuch = handbuch;
    // }
}
