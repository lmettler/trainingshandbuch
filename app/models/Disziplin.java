package models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "Disziplin")
public class Disziplin {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long disziplinId;
    private String disziplinName;
    private long schwerpunktId;

    public long getDisziplinId() {
        return disziplinId;
    }

    public void setDisziplinId(long disziplinId) {
        this.disziplinId = disziplinId;
    }

    public String getDisziplinName() {
        return disziplinName;
    }

    public void setDisziplinName(String disziplinName) {
        this.disziplinName = disziplinName;
    }

    public long getSchwerpunktId() {
        return schwerpunktId;
    }

    public void setSchwerpunktId(long schwerpunktId) {
        this.schwerpunktId = schwerpunktId;
    }
}
