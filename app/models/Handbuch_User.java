package models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "handbuch_user")
public class Handbuch_User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long handbuch_userId;
    private long userId;
    private long handbuchId;

    public long getHandbuch_userId() {
        return handbuch_userId;
    }

    public void setHandbuch_userId(long handbuch_userId) {
        this.handbuch_userId = handbuch_userId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getHandbuchId() {
        return handbuchId;
    }

    public void setHandbuchId(long handbuchId) {
        this.handbuchId = handbuchId;
    }
}
