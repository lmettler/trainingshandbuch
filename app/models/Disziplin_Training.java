package models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "disziplin_training")
public class Disziplin_Training {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long disziplin_trainingId;
    private long trainingId;
    private long disziplinId;

    public long getDisziplin_trainingId() {
        return disziplin_trainingId;
    }

    public void setDisziplin_trainingId(long disziplin_trainingId) {
        this.disziplin_trainingId = disziplin_trainingId;
    }

    public long getTrainingId() {
        return trainingId;
    }

    public void setTrainingId(long trainingId) {
        this.trainingId = trainingId;
    }

    public long getDisziplinId() {
        return disziplinId;
    }

    public void setDisziplinId(long disziplinId) {
        this.disziplinId = disziplinId;
    }
}
