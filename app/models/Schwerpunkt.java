package models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "Schwerpunkt")
public class Schwerpunkt {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long schwerpunktId;
    private String schwerpunktName;

    public long getSchwerpunktId() {
        return schwerpunktId;
    }

    public void setSchwerpunktId(long schwerpunktId) {
        this.schwerpunktId = schwerpunktId;
    }

    public String getSchwerpunktName() {
        return schwerpunktName;
    }

    public void setSchwerpunktName(String schwerpunktName) {
        this.schwerpunktName = schwerpunktName;
    }
}
