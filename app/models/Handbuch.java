package models;

import javax.persistence.*;
import javax.persistence.ManyToMany;
import java.util.Date;
import java.util.Set;

@Entity(name = "Handbuch")
public class Handbuch {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long handbuchId;
    private String gruppenname;
    @Basic
    @Temporal(TemporalType.DATE)
    private Date startdatum;
    @Basic
    @Temporal(TemporalType.DATE)
    private Date enddatum;
    private String trainingstag;
    private String turnverein;
    // @ManyToMany(mappedBy="handbuch")
    // private Set<User> user;

    public long getHandbuchId() {
        return handbuchId;
    }

    public void setHandbuchId(long handbuchId) {
        this.handbuchId = handbuchId;
    }

    public String getGruppenname() {
        return gruppenname;
    }

    public void setGruppenname(String gruppenname) {
        this.gruppenname = gruppenname;
    }

    public Date getStartdatum() {
        return startdatum;
    }

    public void setStartdatum(Date startdatum) {
        this.startdatum = startdatum;
    }

    public Date getEnddatum() {
        return enddatum;
    }

    public void setEnddatum(Date enddatum) {
        this.enddatum = enddatum;
    }

    public String getTrainingstag() {
        return trainingstag;
    }

    public void setTrainingstag(String trainingstag) {
        this.trainingstag = trainingstag;
    }

    public String getTurnverein() {
        return turnverein;
    }

    public void setTurnverein(String turnverein) {
        this.turnverein = turnverein;
    }

}
