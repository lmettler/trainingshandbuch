package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.Handbuch;
import models.User;
import models.Handbuch_User;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import services.HandbuchService;

import javax.inject.Inject;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;


public class HandbuchController extends Controller {


    private final HandbuchService handbuchService;

    @Inject
    public HandbuchController(HandbuchService handbuchService) {
        this.handbuchService = handbuchService;
    }

    public CompletionStage<Result> getAllHandbuecherIndex(String q) {
        return handbuchService.get().thenApplyAsync(handbuchStream ->
                ok(Json.toJson(handbuchStream.collect(Collectors.toList())))
        );
    }

    public CompletionStage<Result> getHandbuchUser(Long id) {
        return handbuchService.getHandbuchUser(id).thenApplyAsync(handbuch -> ok(Json.toJson(handbuch)));
    }

    public CompletionStage<Result> getHandbuch(Long id) {
        return handbuchService.getHandbuch(id).thenApplyAsync(handbuch -> ok(Json.toJson(handbuch)));
    }

    public CompletionStage<Result> addHandbuch(Http.Request request) {
        JsonNode json = request.body().asJson();
        Handbuch AddHandbuch = Json.fromJson(json, Handbuch.class);
        return handbuchService.addHandbuch(AddHandbuch).thenApplyAsync(handbuch -> ok(Json.toJson(handbuch)));
    }

    public CompletionStage<Result> addHandbuchUser(Http.Request request) {
        JsonNode json = request.body().asJson();
        Handbuch_User AddHandbuchUser = Json.fromJson(json, Handbuch_User.class);
        return handbuchService.addHandbuchUser(AddHandbuchUser).thenApplyAsync(hu -> ok(Json.toJson(hu)));
    }

    // public CompletionStage<Result> updateHandbuch(Long id, Http.Request request) {
    //     JsonNode json = request.body().asJson();
    //     Handbuch updateHandbuch = Json.fromJson(json, Handbuch.class);
    //     updateHandbuch.setHandbuchId(id);
    //     return handbuchService.update(updateHandbuch).thenApplyAsync(handbuch -> ok(Json.toJson(handbuch)));
    // }

    public CompletionStage<Result> deleteHandbuch(Long id) {
        return handbuchService.delete(id).thenApplyAsync(removed -> removed ? ok() : internalServerError());
    }

    public CompletionStage<Result> getTraining(Long handbuchId) {
        return handbuchService.getTraining(handbuchId).thenApplyAsync(training -> ok(Json.toJson(training)));
    }
}

