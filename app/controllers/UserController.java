package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.Handbuch;
import models.User;
import models.Handbuch_User;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import services.UserService;

import javax.inject.Inject;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;


public class UserController extends Controller {

    private final UserService userservice;

    @Inject
    public UserController(UserService userservice) {
        this.userservice = userservice;
    }

    public CompletionStage<Result> addUser(Http.Request request) {
        JsonNode json = request.body().asJson();
        User AddUser = Json.fromJson(json, User.class);
        return userservice.addUser(AddUser).thenApplyAsync(user -> ok(Json.toJson(user)));
    }

    public CompletionStage<Result> getUser(Http.Request request) {
        JsonNode json = request.body().asJson();
        User GetUser = Json.fromJson(json, User.class);
        // CompletionStage<User> exists userservice.getUser(GetUser);
        return userservice.getUser(GetUser).thenApplyAsync(user -> ok(Json.toJson(user)));
    }
}