package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.Training;
import models.Disziplin;
import models.Disziplin_Training;
import models.Schwerpunkt;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import services.TrainingService;

import javax.inject.Inject;
import java.util.Iterator;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TrainingController extends Controller {

    private final TrainingService trainingService;

    @Inject
    public TrainingController(TrainingService trainingService) {
        this.trainingService = trainingService;
    }

    public CompletionStage<Result> getAllTrainingIndex(String q) {
        return trainingService.get()
                .thenApplyAsync(trainingStream -> ok(Json.toJson(trainingStream.collect(Collectors.toList()))));
    }

    public CompletionStage<Result> getTraining(Long id) {
        return trainingService.get(id).thenApplyAsync(training -> ok(Json.toJson(training)));
    }

    public CompletionStage<Result> addTraining(Http.Request request) {
        JsonNode json = request.body().asJson();
        Training AddTraining = Json.fromJson(json, Training.class);
        return trainingService.addTraining(AddTraining).thenApplyAsync(training -> ok(Json.toJson(training)));
    }

    public CompletionStage<Result> addDisziplinTraining(Http.Request request) {
        JsonNode json = request.body().asJson();
        Disziplin_Training AddDisziplinTraining = Json.fromJson(json, Disziplin_Training.class);
        return trainingService.addDisziplinTraining(AddDisziplinTraining).thenApplyAsync(dt -> ok(Json.toJson(dt)));
    }

    public CompletionStage<Result> updateTraining(Http.Request request) {
        JsonNode json = request.body().asJson();
        Training updateTraining = Json.fromJson(json, Training.class);
        // updateTraining.setTrainingId(id);
        return trainingService.update(updateTraining).thenApplyAsync(training -> ok(Json.toJson(training)));
    }

    public CompletionStage<Result> deleteTraining(Long id) {
        return trainingService.delete(id).thenApplyAsync(removed -> removed ? ok() : internalServerError());
    }

    public CompletionStage<Result> deleteTrainingDisziplin(Http.Request request) {
        JsonNode json = request.body().asJson();
        Disziplin_Training deletDisziplin = Json.fromJson(json, Disziplin_Training.class);
        return trainingService.deleteTrainingDisziplin(deletDisziplin).thenApplyAsync(removed -> removed ? ok() : internalServerError());
    }

    public CompletionStage<Result> getSchwerpunkt() {
        return trainingService.getSchwerpunkt()
                .thenApplyAsync(schwerpunktStream -> ok(Json.toJson(schwerpunktStream.collect(Collectors.toList()))));
    }

    public CompletionStage<Result> getAllDisziplin() {
        return trainingService.getAllDisziplin()
                .thenApplyAsync(disziplinStream -> ok(Json.toJson(disziplinStream.collect(Collectors.toList()))));
    }

    public CompletionStage<Result> getDisziplin(Long schwerpunktId) {
        return trainingService.getDisziplin(schwerpunktId)
                .thenApplyAsync(disziplin -> ok(Json.toJson(disziplin.collect(Collectors.toList()))));
    }

    public CompletionStage<Result> getDisziplinen(Long trainingId) {
        return trainingService.getDisziplinen(trainingId)
                .thenApplyAsync(disziplin -> ok(Json.toJson(disziplin.collect(Collectors.toList()))));
    }
}

