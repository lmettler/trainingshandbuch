name := """trainingshandbuch"""
organization := "-"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)
herokuAppName in Compile := "trainingshandbuch"
herokuJdkVersion in Compile := "11"
scalaVersion := "2.13.1"

libraryDependencies ++= Seq(
  javaJdbc,
  evolutions,
  jdbc,
  javaJpa,
  javaWs,
  ehcache,
  guice,
  "org.mindrot" % "jbcrypt" % "0.3m",
  "org.hibernate" % "hibernate-core" % "5.4.9.Final",
  "org.postgresql"  % "postgresql"      % "42.2.1",
  "org.mockito" % "mockito-core" % "2.10.0" % "test"

)

//"org.postgresql"  % "postgresql"      % "9.4-1201-jdbc41",