import React, {Component} from 'react';
import {Navbar, Nav, Button} from 'react-bootstrap';
import Spinner from "react-bootstrap/Spinner";
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import axios from 'axios';


import GrobPlanung from "./grobPlanung";
import TerminUebersicht from "./terminUebersicht";
import DetailPlanung from "./detailPlanung";

import AuthService from "./services/auth.service";

import Login from "./components/login.component";
import SignUp from "./components/register.component";
import Profile from "./components/profile.component";
import HandbuchOption from './components/handbuchoption.component';
import AddTraining from "./components/AddTraining.component";

class App extends Component {
    constructor(props) {
        super(props);
        this.logOut = this.logOut.bind(this);
        this.handleSelect = this.handleSelect.bind(this);
        this.reloadTrainings = this.reloadTrainings.bind(this);

        this.state = {
            showAdminBoard: false,
            currentUser: undefined,
            handbuch: [],
            trainings: [],
            reload: true,
            selectedTrainingDays: [],
            selectedDate: [], //für neues Traning erstellen
            trainingArt: "Training",  //für neues Traning erstellen
            showHide: false, //für neues Traning erstellen
        };
    }

    async componentDidMount() {
        const user = await AuthService.getCurrentUser();

        if (user) {
            this.setState({
                currentUser: user,

            });
            this.loadHanduch();
        }
        let id = parseInt(localStorage.getItem('selecthandbuch'));
        this.reloadTrainings(id);
    }

    reloadTrainings = (id) => {
        axios
            .get(`/api/handbuch/training/` + id)
            .then(res => {
                this.setState({trainings: res.data})
                let newObj = this.loadTrainingDays(res.data);
                this.setState({selectedTrainingDays: newObj});
            })
            .catch(err => {
                console.log(err);
            });
    }

    loadTrainingDays = (trainingDays) => {
        let newObj = trainingDays.map(training => {
            return training.trainingDatum;
        })
        return newObj;
    }

    async loadHanduch() {
        const user = await AuthService.getCurrentUser()
        await axios
            .get('api/handbuch/user/' + user.id)
            .then(res => {
                this.setState({handbuch: res.data})
                if (!localStorage.getItem('selecthandbuch')) {
                    localStorage.setItem('selecthandbuch', res.data[0].handbuchId)
                }

            })
            .catch(err => {
                console.error(err);
            });
    }

    addTraining = (event) => {
        event.preventDefault();

        for (let i = 0; i < this.state.selectedDate.length; i++) {
            const requestData = {
                handbuchId: parseInt(localStorage.getItem('selecthandbuch')),
                trainingDatum: this.state.selectedDate[i],
                trainingArt: this.state.trainingArt
            };
            axios
                .post("/api/training", requestData)
                .then(res => {
                    console.log("Response submit " + res.data);
                    this.setState(state => {
                        const training = [...state.trainings, state.selectedDate[i]];
                        this.reloadTrainings(localStorage.getItem('selecthandbuch'))
                        return {
                            training
                        };
                    });
                    this.setState(state => {
                        const training = [...state.selectedTrainingDays, state.selectedTrainingDays[i]];
                        return {
                            training
                        };
                    });
                })
                .catch(err => {
                    console.log(err);
                });
        }
        this.handleModalShowHide();
    };

    addHandbuch = (handbuch) => {
        axios
            .post("/api/handbuch", handbuch)
            .then(res => {
                let obj = {
                    userId: parseInt(this.state.currentUser.id),
                    handbuchId: res.data.handbuchId,
                }
                axios
                    .post("/api/handbuch/user", obj)
                    .then(res => {
                        this.loadHanduch();
                    })
                    .catch(err => {
                        console.error(err);
                    })
            })
            .catch(err => {
                console.error(err);
            })

    }

    handleModalShowHide = () => {
        this.setState({showHide: !this.state.showHide})
    }

    handleDateChange = (date) => {
        this.setState({selectedDate: date})
    };

    //Handbuchauswahl abspeichern
    handleSelect(event) {
        localStorage.removeItem("selecthandbuch");
        localStorage.setItem("selecthandbuch", event.target.value);
        this.setState({reload: !this.state.reload})
        this.reloadTrainings(event.target.value);
    }

    logOut() {
        AuthService.logout();
        localStorage.removeItem("selecthandbuch");
    }

    render() {
        const {currentUser, showAdminBoard} = this.state;
        return (
            <Router>
                <div className="App">
                    <header className="App-header">
                        <Navbar bg="white" expand="lg" className="navbar">
                            <Navbar.Brand>Traningshandbuch</Navbar.Brand>
                            <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                            <Navbar.Collapse id="basic-navbar-nav">
                                {currentUser ? (
                                    <Nav className="mr-auto">
                                        <Nav.Link href="/detailPlanung">Detail Planung</Nav.Link>
                                        <Nav.Link href="/grobPlanung">Grob Planung</Nav.Link>
                                        <Nav.Link href="/terminUebersicht">Terminübersicht</Nav.Link>
                                        <Nav.Link href="/profile"> Profil</Nav.Link>
                                        <Nav.Link href="/sign-in" className="nav-link" onClick={this.logOut}>
                                            LogOut
                                        </Nav.Link>
                                    </Nav>

                                ) : (
                                    <Nav className="mr-auto">
                                        <Nav.Link href="/sign-in">login</Nav.Link>
                                        <Nav.Link href="/sign-up">sign up</Nav.Link>
                                    </Nav>

                                )}
                                {currentUser ? (
                                    <HandbuchOption handleSelect={this.handleSelect} handbuch={this.state.handbuch}/>
                                ) : (
                                    <div></div>
                                )}
                            </Navbar.Collapse>
                        </Navbar>
                        <div className="Content">
                            <Switch>
                                <Route exact path='/' component={Login}/>
                                <Route path="/detailPlanung">
                                    {this.state.trainings.length > 0 ?
                                        <DetailPlanung trainings={this.state.trainings}
                                                       reloadTrainings={this.reloadTrainings}/>
                                        :
                                        <div>
                                            <h3>Loading</h3>
                                            <div className="spinnerContainer">
                                                <Spinner animation="border" role="status" aria-label="Ladeanimation"/>
                                            </div>
                                            <p>&Uuml;berpr&uuml;fen Sie ihre Internetverbindung oder f&uuml;gen Sie ein
                                                Training hinzu, wenn Sie diese Meldung l&auml;nger als 30 Sekunden
                                                sehen.</p>
                                            <Button onClick={this.handleModalShowHide}>Training hinzufügen</Button>
                                            <AddTraining showHide={this.state.showHide}
                                                         handleModalShowHide={this.handleModalShowHide}
                                                         handleDateChange={this.handleDateChange}
                                                         addTraining={this.addTraining}/>
                                        </div>
                                    }
                                </Route>
                                <Route path="/grobPlanung/">
                                    {this.state.selectedTrainingDays.length && this.state.trainings.length > 0 ?
                                        <GrobPlanung trainings={this.state.trainings}
                                                     selectedTrainingDays={this.state.selectedTrainingDays}
                                                     showHide={this.state.showHide}
                                                     handleModalShowHide={this.handleModalShowHide}
                                                     handleDateChange={this.handleDateChange}
                                                     addTraining={this.addTraining}
                                        />
                                        :
                                        <div>
                                            <h3>Loading</h3>
                                            <div className="spinnerContainer">
                                                <Spinner animation="border" role="status" aria-label="Ladeanimation"/>
                                            </div>
                                            <p>&Uuml;berpr&uuml;fen Sie ihre Internetverbindung oder f&uuml;gen Sie ein
                                                Training hinzu, wenn Sie diese Meldung l&auml;nger als 30 Sekunden
                                                sehen.</p>
                                            <Button onClick={this.handleModalShowHide}>Training hinzufügen</Button>
                                            <AddTraining showHide={this.state.showHide}
                                                         handleModalShowHide={this.handleModalShowHide}
                                                         handleDateChange={this.handleDateChange}
                                                         addTraining={this.addTraining}/>
                                        </div>
                                    }
                                </Route>

                                <Route path="/terminUebersicht" component={TerminUebersicht}/>
                                <Route path="/sign-in" component={Login}/>
                                <Route path="/sign-up" component={SignUp}/>
                                <Route exact path="/profile">
                                    <Profile addHandbuch={this.addHandbuch} handbuch={this.state.handbuch}/>
                                </Route>
                            </Switch>
                        </div>
                    </header>
                </div>
            </Router>

        );
    }
}

export default App;
