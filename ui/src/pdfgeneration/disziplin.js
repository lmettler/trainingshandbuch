import {Paragraph, TextRun, TableCell} from "docx";
import axios from "axios";

class Title {

    async getDisziplin(trainingId) {
        const fullruns = await axios
            .get('/api/training/disziplin/' + trainingId)
            .then(res => {
                var textRuns = []
                textRuns.push(new TextRun("Disziplinen: "))
                res.data.forEach(element => {
                    const textRun = new TextRun(
                        element.disziplinName + ",  "
                    );
                    textRuns.push(textRun);
                });
                return textRuns;

            })
            .catch(err => {
                console.error(err);
            })

        const cell =
            new TableCell({
                children: [new Paragraph({
                    children: fullruns,
                    style: "heading2"
                })],
                margins: {
                    top: 100,
                    bottom: 100,
                    left: 100,
                    right: 100,
                },
            })

        return (cell);
    }
}

export default new Title();