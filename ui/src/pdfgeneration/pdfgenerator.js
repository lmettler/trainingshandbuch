import {saveAs} from "file-saver";
import * as docx from "docx";
import {Paragraph, Packer, AlignmentType, PageBreak} from "docx";
import Title from "./title";
import Training from "./training";
import axios from "axios";

//Erzeuge PDF mit pdfkit
class Generatepdf {
    async generate(handbuchId) {
        const handbuch =
            await axios
                .get('/api/handbuch/' + handbuchId)
                .then(res => {
                    console.log(res.data);
                    return res.data;
                })
                .catch(err => {
                    console.error(err);
                })

        const training =
            await axios
                .get('/api/handbuch/training/' + handbuch.handbuchId)
                .then(res => {
                    return res.data;
                })
                .catch(err => {
                    console.error(err);
                })

        const doc = new docx.Document({
            title: "Trainingshandbuch: " + handbuch.gruppenname + " " + handbuch.startdatum + " - " + handbuch.enddatum,
            styles: {
                paragraphStyles: [
                    {
                        id: "title",
                        name: "Title",
                        paragraph: {
                            alignment: AlignmentType.CENTER,
                        },
                        run: {
                            font: "Calibri",
                            size: 60,
                            bold: true
                        },
                    },
                    {
                        id: "heading1",
                        name: "Heading1",
                        run: {
                            font: "Calibri",
                            size: 30,
                            bold: true
                        }
                    },
                    {
                        id: "heading2",
                        name: "Heading2",
                        run: {
                            font: "Calibri",
                            size: 26,
                            bold: true
                        }
                    },
                    {
                        id: "text",
                        name: "Text",
                        run: {
                            font: "Calibri",
                            size: 26,
                            bold: false
                        }
                    },
                ],
            },
        });

        const docchildrens = Title.getTitle(handbuch);
        for (const element of training) {
            const trainingblock = await Training.getTraining(element);
            docchildrens.push(trainingblock);
            docchildrens.push(new Paragraph({children: [new PageBreak()]}));
        }
        ;
        doc.addSection({
            properties: {},
            children: docchildrens
        });

        Packer.toBlob(doc).then((blob) => {
            saveAs(blob, "Trainingshandbuch: " + handbuch.gruppenname);
        });

    }

}

export default new Generatepdf();