import {
    Paragraph, HeadingLevel, AlignmentType,
    Table, TableRow, TableCell
} from "docx";

class Title {

    getTitle(handbuch) {
        const elements = [];

        const titel = new Paragraph({
            text: "Trainingshandbuch: " +
                handbuch.startdatum.slice(0, 4) + " - "
                + handbuch.enddatum.slice(0, 4) + " "
                + handbuch.gruppenname,
            heading: HeadingLevel.TITLE,
        });
        elements.push(titel);

        const table1 = new Table({
            rows: [
                new TableRow({
                    children: [
                        new TableCell({
                            children: [new Paragraph({
                                text: "Turnverein: " + handbuch.turnverein,
                                style: "heading1"
                            })],
                            columnSpan: 10,
                            margins: {
                                top: 200,
                                bottom: 200,
                                left: 200,
                                right: 200,
                            },
                            borders: {
                                bottom: {color: "white"},
                                top: {color: "white"},
                                left: {color: "white"},
                                right: {color: "white"},
                            }
                        }),
                        new TableCell({
                            children: [new Paragraph({
                                text: "Trainingstag: " + handbuch.trainingstag,
                                style: "heading1"
                            })],
                            columnSpan: 10,
                            margins: {
                                top: 200,
                                bottom: 200,
                                left: 200,
                                right: 200,
                            },
                            borders: {
                                bottom: {color: "white"},
                                top: {color: "white"},
                                left: {color: "white"},
                                right: {color: "white"},
                            }
                        }),
                    ],
                }),
            ],
            alignment: AlignmentType.CENTER,
        });
        elements.push(table1);
        const par = new Paragraph("");
        elements.push(par);
        return (elements);
    }
}

export default new Title();