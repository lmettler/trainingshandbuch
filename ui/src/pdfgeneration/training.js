import {
    Paragraph, AlignmentType,
    Table, TableRow, TableCell, WidthType
} from "docx";
import Disziplin from "./disziplin";
import Details from "./details";


class Training {

    async getTraining(training) {
        const elements = [];
        const disziplinen = await Disziplin.getDisziplin(training.trainingId);

        const trainingtable = new Table({
            rows: [
                new TableRow({
                    children: [
                        new TableCell({
                            children: [new Paragraph({
                                text: "Training: " + training.trainingDatum,
                                style: "heading1"
                            })],
                            margins: {
                                top: 200,
                                bottom: 200,
                                left: 200,
                                right: 200,
                            },
                        }),
                    ],
                }),
                new TableRow({
                    children: [disziplinen]
                }),
                Details.getDetails("Einleitung", training.einleitung),
                Details.getDetails("Hauptteil", training.hauptteil),
                Details.getDetails("Schluss", training.schluss),
                Details.getDetails("Bemerkungen", training.bemerkungen),
            ],
            alignment: AlignmentType.CENTER,
            width: {
                size: 100,
                type: WidthType.PERCENTAGE
            },
            spacing: {
                after: 120
            }
        });
        elements.push(trainingtable);
        return (trainingtable);
    }
}

export default new Training();