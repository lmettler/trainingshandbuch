import {Paragraph, TableRow, TableCell} from "docx";


class Details {

    getDetails(head, text) {

        const row = new TableRow({
            children: [
                new TableCell({
                    children: [
                        new Paragraph({
                            text: head,
                            style: "heading2"
                        }),
                        new Paragraph({
                            text: text,
                            style: "text"
                        })
                    ],
                    margins: {
                        top: 200,
                        bottom: 200,
                        left: 200,
                        right: 200,
                    },
                }),
            ],
        });
        return (row);
    };

}

export default new Details();