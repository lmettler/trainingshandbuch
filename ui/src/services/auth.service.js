import axios from "axios";

const API_URL = "/api/auth/";
const bcrypt = require('bcryptjs');
const salt = bcrypt.genSaltSync(10);
var jwt = require("jsonwebtoken");

class AuthService {

    async login(email, password) {
        let resMessage = "";
        return axios
            .post(API_URL + "signin", {
                email: email
            })
            .then(response => {
                console.log(response.data);
                if (!response) {
                    //resMessage = "Benutzer nicht gefunden";
                    //return resMessage;
                }

                let go = bcrypt.compareSync(password, response.data.passwort);
                if (go) {
                    console.log('Submitted password is correct');
                    let ha = process.env.ENV_VARIABLESEC; // für die Backendverwendung  als Lokale Variable verwenden lokale variabel im Herokudeployment schon angelegt
                    ha = "upersecretmonsterjwt";
                    console.log(ha);
                    let token = jwt.sign({id: response.data.userId}, ha, {
                        expiresIn: 9999999 // 33min    ****************
                    });
                    let authorities = [];
                    authorities.push("ROLE_ADMIN");

                    let data =
                        {
                            id: response.data.userId,
                            vorname: response.data.vorname,
                            nachname: response.data.nachname,
                            turnverein: response.data.turnverein,
                            roles: authorities,
                            accessToken: token,
                        }
                    localStorage.setItem("user", JSON.stringify(data));
                    console.log("compare true");
                    console.log(token);

                } else {
                    //   resMessage = "Passwort / Mail kombination falsch";
                    //  return resMessage;
                }
                if (response.data.accessToken) {
                    localStorage.setItem("user", JSON.stringify(response.data));
                }
                return response.data;
            });
    }

    logout() {
        localStorage.removeItem("user");
    }

    async register(prename, username, mail, password, vereinname) {
        const passend = await bcrypt.hash(password, salt);
        return axios.post(API_URL + "signup", {
            vorname: prename,
            nachname: username,
            passwort: passend,
            email: mail,
            turnverein: vereinname
        });
    }

    getCurrentUser() {
        let user = JSON.parse(localStorage.getItem('user'));
        if (user) {
            let token = user.accessToken;
            // console.log(token);
            let ha = process.env.ENV_VARIABLESEC; // in heroku definiert
            ha = "upersecretmonsterjwt";
            jwt.verify(token, ha, (err, decoded) => {
                if (err) {
                    localStorage.removeItem("user");
                }
            });
            return JSON.parse(localStorage.getItem('user'));
        }
    }
}

export default new AuthService();
