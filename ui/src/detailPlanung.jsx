import React, {Component} from "react";
import {Form, Button} from "react-bootstrap";
import axios from 'axios';
import './detailPlanung.css';
import TrainingList from "./components/traininglist.component";
import AuthService from "./services/auth.service";
import {Redirect} from "react-router-dom";


export default class DetailPlanung extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedTraining: "0",
            training: [],               //alle Daten des aktuellen Training
            trainDisz: [],
            einleitung: "",
            hauptteil: "",
            schluss: "",
            bemerkungen: "",
            redirect: null,
        };
    }

    componentDidMount() {
        const currentUser = AuthService.getCurrentUser();
        if (!currentUser) this.setState({redirect: "/sign-in"});
    }

    loadTrainingData = (obj) => {
        Object.keys(obj).forEach((key) => {
            if (obj[key] === null) {
                obj[key] = "";
            }
        })
        return obj;
    }

    handleTrainingChange = (event) => {
        this.setState({trainDisz: []});
        this.setState({training: []});
        this.setState({selectedTraining: event.target.value})
        this.relodDisziplin(event.target.value);
    };

    handleInputChange = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    handleRemoveDisz = (event) => {
        event.preventDefault();

        const requestData = {
            trainingId: parseInt(this.state.selectedTraining),
            disziplinId: parseInt(event.currentTarget.attributes[0].nodeValue)
        };
        axios
            .delete("/api/disziplin", {data: requestData})
            .then(res => {
                this.relodDisziplin(this.state.selectedTraining);
            })
            .catch(err => {
                console.log(err);
            });
    }

    relodDisziplin = (training) => {
        const trainingRequest = axios.get(`/api/training/${training}`);
        const trainDiszRequest = axios.get(`/api/training/disziplin/${training}`);

        axios
            .all([trainingRequest, trainDiszRequest])
            .then(axios.spread((...res) => {
                const resTraining = res[0]
                const resTrainDisz = res[1]

                this.setState({training: resTraining.data})
                this.setState({trainDisz: resTrainDisz.data})

                let newObj = this.loadTrainingData(this.state.training);
                this.setState({
                    einleitung: newObj.einleitung,
                    hauptteil: newObj.hauptteil,
                    schluss: newObj.schluss,
                    bemerkungen: newObj.bemerkungen
                });

            }))
            .catch(errors => {
                console.log(errors)
            });
    }

    handleSubmit = (event) => {
        event.preventDefault();
        const requestData = {
            trainingId: parseInt(this.state.selectedTraining),
            handbuchId: this.state.training.handbuchId,
            trainingDatum: this.state.training.trainingDatum,
            trainingArt: this.state.training.trainingArt,
            einleitung: this.state.einleitung,
            hauptteil: this.state.hauptteil,
            schluss: this.state.schluss,
            bemerkungen: this.state.bemerkungen
        }
        axios
            .put(`/api/training`, requestData)
            .then(res => {
                console.log("Submited data" + res.data);
            })
            .catch(err => {
                console.log(err);
            });
    }

    deleteTraining = (event) => {
        event.preventDefault();
        axios
            .delete(`/api/training/${event.target.value}`)
            .then(res => {
                let g = parseInt(localStorage.getItem('selecthandbuch'));
                this.props.reloadTrainings(g);

                this.setState({
                    einleitung: "",
                    hauptteil: "",
                    schluss: "",
                    bemerkungen: "",
                    trainDisz: [],
                })
            })
            .catch(err => {
                console.error(err);
            })
    }


    render() {
        if (this.state.redirect) {
            return <Redirect to={this.state.redirect}/>
        }
        return (
            <form onSubmit={this.handleSubmit}>
                <h3>Detail Planung</h3>

                <TrainingList trainings={this.props.trainings} handleTrainingChange={this.handleTrainingChange}
                              trainDisz={this.state.trainDisz} handleRemoveDisz={this.handleRemoveDisz}
                              selectedTraining={this.state.selectedTraining}/>
                {this.state.selectedTraining === "0" ?
                    (
                        <div></div>
                    ) : (
                        <div>
                            <Form.Group>
                                <Form.Label>Einleitung</Form.Label>
                                <Form.Control type="text" id="inputEinleitung" name="einleitung" size="sm"
                                              label={"Einleitung"} value={this.state.einleitung}
                                              onChange={this.handleInputChange}/>
                            </Form.Group>
                            <Form.Group>
                                <Form.Label>Hauptteil</Form.Label>
                                <Form.Control as='textarea' id="inputHauptteil" name="hauptteil" rows={3}
                                              value={this.state.hauptteil} onChange={this.handleInputChange}/>
                            </Form.Group>
                            <Form.Group>
                                <Form.Label>Schluss</Form.Label>
                                <Form.Control type="text" id="inputSchluss" name="schluss" size="sm"
                                              value={this.state.schluss} onChange={this.handleInputChange}/>
                            </Form.Group>
                            <Form.Group>
                                <Form.Label>Bemerkungen</Form.Label>
                                <Form.Control as='textarea' id="inputBemerkungen" name="bemerkungen" rows={3}
                                              value={this.state.bemerkungen} onChange={this.handleInputChange}/>
                            </Form.Group>
                            <Button type="submit" className="submitBtn">Training aktualisieren</Button>
                            <Button className="submitBtn" variant="outline-danger" onClick={this.deleteTraining}
                                    value={this.state.selectedTraining}>Training Löschen</Button>
                        </div>
                    )}
            </form>
        );
    }
}
