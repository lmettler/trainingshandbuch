import React, { Component } from "react";
import {Form} from "react-bootstrap";

export default class SchwerpunktListe extends Component {


    render() {
        const sList = this.props.schwerpunkt.map((schwerpunkte) => {
            return (
                <option key={schwerpunkte.schwerpunktId} value={`${schwerpunkte.schwerpunktId}`} >{schwerpunkte.schwerpunktName}</option>
            )
        })

        return (
            <div className="TrainingauswahlDetail">
                <div>Schwerpunkt</div>
                <Form.Group className="trainingSelectDetail">
                    <Form.Control as="select" size="sm" value={this.props.selectedSchwerpunkt} onChange={this.props.handleSchwerpunktChange.bind(this)}>
                        {sList}
                    </Form.Control>
                </Form.Group>
            </div>
        );
    }
}
