import React, {Component} from "react";
import {Redirect} from "react-router-dom";
import {Button} from 'react-bootstrap';
import AuthService from "../services/auth.service";
import AddHandbuch from "./AddHandbuch.component";
import PrintHandbuch from "./PrintHandbuch.component";
import kim from "../images/kim.jpg";

export default class Profile extends Component {
    constructor(props) {
        super(props);

        this.state = {
            redirect: null,
            userReady: false,
            currentUser: {username: ""},
            showHideAdd: false,
            showHidePrint: false,
        };
    }

    componentDidMount() {
        const currentUser = AuthService.getCurrentUser();
        if (!currentUser) this.setState({redirect: "/sign-in"});
        this.setState({currentUser: currentUser, userReady: true})
    }

    handleAddModalShowHide = () => {
        this.setState({showHideAdd: !this.state.showHideAdd})
    }

    handlePrintModalShowHide = () => {
        this.setState({showHidePrint: !this.state.showHidePrint})
    }

    render() {
        if (this.state.redirect) {
            return <Redirect to={this.state.redirect}/>
        }

        const {currentUser} = this.state;

        return (
            <div className="container">
                {(this.state.userReady) ?
                    <div>
                        <header className="jumbotron">
                            <img src={kim} alt="kimtheking"/>
                        </header>
                        <p>
                            <strong>Vorname:</strong>{" "}
                            {currentUser.vorname}
                        </p>
                        <p>
                            <strong>Nachname:</strong>{" "}
                            {currentUser.nachname}
                        </p>

                        <p>
                            <strong>Turnverein:</strong>{" "}
                            {currentUser.turnverein}
                        </p>
                        <Button className="submitBtn" onClick={this.handleAddModalShowHide}>Neues Handbuch</Button>
                        <Button className="submitBtn" onClick={this.handlePrintModalShowHide}>Handbuch Drucken</Button>
                        <AddHandbuch showHideAdd={this.state.showHideAdd}
                                     handleAddModalShowHide={this.handleAddModalShowHide}
                                     addHandbuch={this.props.addHandbuch.bind(this)}
                                     turnverein={currentUser.turnverein}/>
                        {
                            this.props.handbuch ?
                                <PrintHandbuch showHidePrint={this.state.showHidePrint}
                                               handlePrintModalShowHide={this.handlePrintModalShowHide}
                                               handbuch={this.props.handbuch}/>
                                :
                                <div></div>
                        }
                    </div>
                    : null
                }
            </div>
        );
    }
}
