import React, { Component } from "react";
import {Form, Button, Modal} from "react-bootstrap";
import PdfGenerator from "../pdfgeneration/pdfgenerator";

export default class AddHandbuch extends Component {
    constructor(props) {
        super(props);

        this.state = {
            submitevent: undefined,
        }
    }
    
    handleSubmit = (event) => {
        event.preventDefault();
        let handbuch = event.target[1].value;
        PdfGenerator.generate(handbuch);
    }

    render() {

        const hList = this.props.handbuch.map((handbuch) => {
            //Select Titel generieren.
            const title = `${handbuch.gruppenname}: ${handbuch.startdatum.slice(0,4)}
            - ${handbuch.enddatum.slice(0,4)}`;
            const handbuchId = handbuch.handbuchId;
            return (
                <option key={handbuchId} value={handbuchId}>{title}</option>
            )
        })

        return (
            <Modal show={this.props.showHidePrint} centered>
                    <Form onSubmit={this.handleSubmit}>
                <Modal.Header closeButton onClick={this.props.handlePrintModalShowHide.bind(this)}>
                    <Modal.Title>Handbuch hinzufügen</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                        <Form.Group>
                            <Form.Label>Handbuch Auswählen</Form.Label>
                            <Form.Control as="select" key={"Trainingstag"} name={"Trainingstag"}>
                                {hList}
                            </Form.Control>
                        </Form.Group>
                </Modal.Body>
                <Modal.Footer>
                    <Button type="submit" onClick={this.props.handlePrintModalShowHide.bind(this)}>
                        Handbuch drucken
                    </Button>
                </Modal.Footer>
                    </Form>
            </Modal>
        );
    }
}
