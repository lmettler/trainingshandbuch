import React, { Component } from "react";
import {Form} from "react-bootstrap";

export default class DisziplinenListe extends Component {

    render() {
        const dList = this.props.disziplinSchwerpunkt.map((disziplinen) => {
            return (
                <option key={disziplinen.disziplinId} value={disziplinen.disziplinId}>{disziplinen.disziplinName}</option>
            )
        })

        return (
            <div className="TrainingauswahlDetail">
                <div>Disziplin</div>
                <Form.Group className="trainingSelectDetail">
                    <Form.Control as="select" size="sm" onChange={this.props.handleDiszChange.bind(this)}>
                    <option key={0} value={0}>Wähle eine Disziplin</option>
                        {dList}
                    </Form.Control>
                </Form.Group>
            </div>
        );
    }
}
