import React, { Component } from "react";
import {Form, Button, Modal} from "react-bootstrap";
import {DatePicker, theme} from 'react-trip-date';
import {ThemeProvider} from 'styled-components';


const handleResponsive = setNumberOfMonth => {
    let width = document.querySelector('.tp-calendar').clientWidth;
    if (width > 900) {
        setNumberOfMonth(2);
    } else if (width < 900 && width > 580) {
        setNumberOfMonth(1);
    } else if (width < 580) {
        setNumberOfMonth(1);
    }
};

const Day = ({day}) => {
    return (
        <>
            <p className="date">{day.format('DD')}</p>
        </>
    );
};

const Title: React.FunctionComponent<{ source: any }> = ({ source }) => {
    let titleDay = ['Sa', 'Fr', 'Do', 'Mi', 'Di', 'Mo', 'So'];
    // you can just change titles, not day start of the week
    return (
        <div className="TitleDaysOfWeekStyle">
            {titleDay.map(item => (
                <p key={Math.random()}>{item}</p>
            ))}
        </div>
    );
};

export default class AddTraining extends Component {

    render() {
        return (
            <Modal show={this.props.showHide} centered>
                <Modal.Header closeButton onClick={this.props.handleModalShowHide.bind(this)}>
                    <Modal.Title>Training hinzufügen</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <form>
                        <Form.Group>
                            <Form.Label>Training oder Wettkampf?</Form.Label>
                            <Form.Control as="select" size="sm" name={"trainingart"} value={this.props.trainingArt} onChange={this.handleChange}>
                                <option key={"training"} value={"Training"} >Training</option>
                                <option key={"wettkampf"} value={"Wettkampf"}>Wettkampf</option>
                            </Form.Control>
                        </Form.Group>
                    </form>
                    <ThemeProvider theme={theme}>
                        <DatePicker
                            handleChange={this.props.handleDateChange.bind(this)}
                            selectedDays={this.props.selectedDate} //initial selected days
                            jalali={false}
                            numberOfMonths={1}
                            numberOfSelectableDays={10} // number of days you need
                            disabledDays={['2020-11-08']} //disabled days
                            responsive={handleResponsive} // custom responsive, when using it, `numberOfMonths` props not working
                            disabledBeforToday={true}
                            disabled={false} // disable calendar
                            dayComponent={Day} //custom day component
                            titleComponent={Title} // custom title of days
                        />
                    </ThemeProvider>
                </Modal.Body>
                <Modal.Footer>
                    <Button   onClick={this.props.addTraining.bind(this)}>
                        Training hinzufügen
                    </Button>
                </Modal.Footer>
            </Modal>
        );
    }
}
