import React, {Component,} from "react";
import {DatePicker, theme} from 'react-trip-date';
import {ThemeProvider} from 'styled-components';


const handleResponsive = setNumberOfMonth => {
    let width = document.querySelector('.tp-calendar').clientWidth;
    if (width > 900) {
        setNumberOfMonth(3);
    } else if (width < 900 && width > 580) {
        setNumberOfMonth(2);
    } else if (width < 580) {
        setNumberOfMonth(1);
    }
};

const Day = ({day}) => {
    return (
        <>
            <p className="date">{day.format('DD')}</p>
        </>
    );
};

const Title: React.FunctionComponent<{ source: any }> = ({ source }) => {
    let titleDay = ['Sa', 'Fr', 'Do', 'Mi', 'Di', 'Mo', 'So'];
    // you can just change titles, not day start of the week
    return (
        <div className="TitleDaysOfWeekStyle">
            {titleDay.map(item => (
                <p key={Math.random()}>{item}</p>
            ))}
        </div>
    );
};

export default class DatepickerComponent extends Component {

    render() {
        return (
            <ThemeProvider theme={theme}>
                <DatePicker
                    selectedDays={this.props.selectedTrainingDays} //initial selected days
                    jalali={false}
                    numberOfMonths={2}
                    numberOfSelectableDays={50} // number of days you need
                    disabledDays={['2020-11-08']} //disabeld days
                    responsive={handleResponsive} // custom responsive, when using it, `numberOfMonths` props not working
                    disabledBeforToday={true}
                    disabled={true} // disable calendar
                    dayComponent={Day} //custom day component
                    titleComponent={Title} // custom title of days
                />
            </ThemeProvider>
        )
    }
}