import React, { Component } from "react";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import { isEmail } from "validator";
import '../index.css';
import { Redirect } from "react-router-dom";
import AuthService from "../services/auth.service";

const required = value => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert">
        This field is required!
      </div>
    );
  }
};

const email = value => {
  if (!isEmail(value)) {
    return (
      <div className="alert alert-danger" role="alert">
        Ungültige Mailaddresse
      </div>
    );
  }
};

const vusername = value => {
  if (value.length < 3 || value.length > 20) {
    return (
        <div className="alert alert-danger" role="alert">
          Der Name muss zwischen 3 und 20 Zeichen sein
        </div>
    );
  }
};
const vverien = value => {
  if (value.length < 3 || value.length > 20) {
    return (
        <div className="alert alert-danger" role="alert">
          Der Vereinsname muss zwischen 3 und 20 Zeichen sein
        </div>
    );
  }
};
const prename = value => {
  if (value.length < 3 || value.length > 20) {
    return (
        <div className="alert alert-danger" role="alert">
          Der Vorname muss zwischen 3 und 20 Zeichen sein
        </div>
    );
  }
};

const vpassword = value => {
  if (value.length < 6 || value.length > 40) {
    return (
      <div className="alert alert-danger" role="alert">
       Das passwort muss zwischen 6 und 40 Zeichen sein
      </div>
    );
  }
};

export default class Register extends Component {
  constructor(props) {
    super(props);
    this.handleRegister = this.handleRegister.bind(this);
    this.onChangeUsername = this.onChangeUsername.bind(this);
    this.onChangeEmail = this.onChangeEmail.bind(this);
    this.onChangePassword = this.onChangePassword.bind(this);
    this.onChangePrename =  this.onChangePrename.bind(this);
    this.onChangeVereinname =  this.onChangeVereinname.bind(this);
    this.state = {
      username: "",
      prename: "",
      mail: "",
      vereinname: "",
      password: "",
      successful: false,
      message: ""
    };
  }

  onChangeUsername(e) {
    this.setState({
      username: e.target.value
    });
  }
  onChangePrename(e) {
    this.setState({
      prename: e.target.value
    });
  }

  onChangeEmail(e) {
    this.setState({
      mail: e.target.value
    });
  }
  onChangeVereinname(e) {
    this.setState({
      vereinname: e.target.value
    });
  }

  onChangePassword(e) {
    this.setState({
      password: e.target.value
    });
  }

  handleRegister(e) {
    e.preventDefault();

    this.setState({
      message: "",
      successful: false
    });

    this.form.validateAll();

    if (this.checkBtn.context._errors.length === 0) {
      AuthService.register(
          this.state.prename,
        this.state.username,
        this.state.mail,
        this.state.password,
       this.state.vereinname
      ).then(
        response => {
          this.setState({
            message: response.data.message,
            successful: true
          });
        },
        error => {
          const resMessage =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();

          this.setState({
            successful: false,
            message: resMessage
          });
        }
      );
    }
  }

  render() {
    const {successful} = this.state;
    if (this.state.successful === true) {
      return <Redirect to={"/sign-in" } />
    }
    return (
      <div className="auth-wrapper">
        <div className="auth-inner">
          <h3>Neues Konto</h3>

          <Form
            onSubmit={this.handleRegister}
            ref={c => {
              this.form = c;
            }}
          >
            {!this.state.successful && (
              <div>
                <div className="form-group">
                  <label htmlFor="username">Name</label>
                  <Input
                    type="text"
                    className="form-control"
                    name="username"
                    value={this.state.username}
                    onChange={this.onChangeUsername}
                    validations={[required, vusername]}
                    placeholder="Strässle"
                  />
                </div>
                <div className="form-group">
                  <label>Vorname</label>
                  <Input
                      type="text"
                      className="form-control"
                      name="username"
                      value={this.state.prename}
                      onChange={this.onChangePrename}
                      validations={[required, prename]}
                      placeholder="Michael"
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="email">Email</label>
                  <Input
                    type="text"
                    className="form-control"
                    name="email"
                    value={this.state.mail}
                    onChange={this.onChangeEmail}
                    validations={[required, email]}
                    placeholder="muster@gmail.com"
                  />
                </div>
                <div className="form-group">
                  <label>Trunverein</label>
                  <Input
                      type="text"
                      className="form-control"
                      name="email"
                      value={this.state.vereinname}
                      onChange={this.onChangeVereinname}
                      validations={[required, vverien]}
                      placeholder="TV Romania"
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="password">Passwort</label>
                  <Input
                    type="password"
                    className="form-control"
                    name="password"
                    value={this.state.password}
                    onChange={this.onChangePassword}
                    validations={[required, vpassword]}
                    placeholder="1234Abc*ç%&/("
                  />
                </div>

                <div className="form-group">
                  <button className="btn btn-primary btn-block">Registrieren</button>
                </div>
              </div>
            )}
            <p className="forgot-password text-right">
              Konto bereits vorhanden <a href="/sign-in">Login</a>
            </p>
            {this.state.message && (
              <div className="form-group">
                <div
                  className={
                    this.state.successful
                      ? "alert alert-success"
                      : "alert alert-danger"
                  }
                  role="alert"
                >
                  {this.state.message}
                </div>
              </div>
            )}
            <CheckButton
              style={{ display: "none" }}
              ref={c => {
                this.checkBtn = c;
              }}
            />
          </Form>
        </div>
      </div>
    );
  }
}
