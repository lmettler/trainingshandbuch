import React, { Component } from "react";
import {XCircle} from "react-bootstrap-icons";

import {Form, Button} from "react-bootstrap";

export default class TrainingList extends Component {
    render() {
        const rawtraining = this.props.trainings;
        const sorttraining = rawtraining.sort(function(a, b){return Date.parse(a.trainingDatum) - Date.parse(b.trainingDatum)})
        const tList = sorttraining.map((trainings) => {
        // const tList = this.props.trainings.map((trainings) => {
            return (
                <option key={trainings.trainingId} value={trainings.trainingId}>{trainings.trainingDatum}</option>
            )
        })
        const DList = this.props.trainDisz.map((disziplin) => {
            return (
                <div key={disziplin.disziplinId}>
                    {disziplin.disziplinName} 
                    <Button value={disziplin.disziplinId} variant="outline-danger" className="removeButton" size="sm" onClick={this.props.handleRemoveDisz.bind(this)}><XCircle size="1rem"/></Button>
                </div>
            )
        })
        return (
            <div>
                <div className="TrainingauswahlDetail">
                    <div>Training vom </div>
                    <Form.Group className="trainingSelectDetail">
                        <Form.Control as="select" size="sm" onChange={this.props.handleTrainingChange.bind(this)}>
                            <option key={0} value={0}>Wähle ein Training</option>
                            {tList}
                        </Form.Control>
                    </Form.Group>
                </div>

                {this.props.selectedTraining === "0" ?
                    (
                        <div></div>
                    ) : (
                        <div>
                            <p><strong><u>Disziplinen</u></strong></p>
                            <div className="disziplindiv">
                                {DList}
                            </div>
                        </div>
                    )}

            </div>
        );
    }
}
