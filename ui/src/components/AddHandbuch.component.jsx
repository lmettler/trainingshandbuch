import React, { Component } from "react";
import {Form, Button, Modal} from "react-bootstrap";


export default class AddHandbuch extends Component {
    constructor(props) {
        super(props);

        this.state = {
            submitevent: undefined,
        }
    }
    
    handleSubmit = (event) => {
        event.preventDefault();
        let obj = {
            gruppenname: event.target[1].value,
            startdatum: event.target[2].value,
            enddatum: event.target[3].value,
            trainingstag: event.target[4].value,
            turnverein: this.props.turnverein,
        }
        this.props.addHandbuch(obj);
    }

    render() {
        return (
            <Modal show={this.props.showHideAdd} centered>
                    <Form onSubmit={this.handleSubmit}>
                <Modal.Header closeButton onClick={this.props.handleAddModalShowHide.bind(this)}>
                    <Modal.Title>Handbuch hinzufügen</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                        <Form.Group>
                            <Form.Label>Gruppenname</Form.Label>
                            <Form.Control type="text" key={"gruppenname"} name={"gruppenname"}></Form.Control>
                            <Form.Label>Startdatum</Form.Label>
                            <Form.Control type="date" key={"startdatum"} name={"startdatum"}></Form.Control>
                            <Form.Label>Enddatum</Form.Label>
                            <Form.Control type="date" key={"Enddatum"} name={"Enddatum"}></Form.Control>
                            <Form.Label>Trainingstag</Form.Label>
                            <Form.Control as="select" key={"Trainingstag"} name={"Trainingstag"}>
                                <option key={"Montag"} value={"Montag"}>Montag</option>
                                <option key={"Dienstag"} value={"Dienstag"}>Dienstag</option>
                                <option key={"Mittwoch"} value={"Mittwoch"}>Mittwoch</option>
                                <option key={"Donnerstag"} value={"Donnerstag"}>Donnerstag</option>
                                <option key={"Freitag"} value={"Freitag"}>Freitag</option>
                                <option key={"Samstag"} value={"Samstag"}>Samstag</option>
                                <option key={"Sonntag"} value={"Sonntag"}>Sonntag</option>
                            </Form.Control>
                        </Form.Group>
                </Modal.Body>
                <Modal.Footer>
                    <Button type="submit" onClick={this.props.handleAddModalShowHide.bind(this)}>
                        Handbuch hinzufügen
                    </Button>
                </Modal.Footer>
                    </Form>
            </Modal>
        );
    }
}
