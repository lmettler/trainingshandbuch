import React, { Component } from "react";
import {Form} from "react-bootstrap";


export default class HandbuchOption extends Component {
    constructor(props) {
        super(props);
        this.state = {
            handbuch: [],
        };
    }

    render() {
        //Aktuell ausgewähltes Handbuch aus localStorage holen
        const selecthandbuch = parseInt(localStorage.getItem('selecthandbuch'));
        //Selektoptionen erstellen
        const hList = this.props.handbuch.map((handbuch) => {
            //Select Titel generieren.
            const title = `${handbuch.gruppenname}: ${handbuch.startdatum.slice(0,4)}
            - ${handbuch.enddatum.slice(0,4)}`;
            const handbuchId = handbuch.handbuchId;
            return (
                <option key={handbuchId} value={handbuchId}>{title}</option>
            )
        })

        return (
            <Form inline>
                <Form.Control onChange={this.props.handleSelect.bind(this)} as="select" className="mr-sm-2 navSelect"
                                value={selecthandbuch}>
                    {hList}
                </Form.Control>
            </Form>
        );
    }
}
