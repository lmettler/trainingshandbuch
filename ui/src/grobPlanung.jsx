import React, {Component,} from "react";
import {Form, Button} from 'react-bootstrap';
import {PlusCircleFill} from "react-bootstrap-icons";
import './grobPlanung.css';

import axios from "axios";
import TrainingList from "./components/traininglist.component";
import DatepickerComponent from "./components/datepicker.component";
import SchwerpunktListe from "./components/schwerpunkte.component";
import DisziplinenListe from "./components/disziplinen.component";
import AddTraining from "./components/AddTraining.component";
import AuthService from "./services/auth.service";
import {Redirect} from "react-router-dom";

export default class grobPlanunug extends Component {
    constructor(props) {
        super(props);
        this.state = {
            schwerpunkt: [],    //alle Schwerpunkte für Liste
            disziplin: [],      //alle Disziplinen für Liste
            trainDisz: [],      //alle Disziplinen des ausgewählten Trainings
            selectedTraining: "0",
            selectedSchwerpunkt: "1",
            disziplinSchwerpunkt: [],
            selectedDisziplin: "",
            selectedDays: [this.props.selectedTrainingDays],  //alle Trainingstage für Datepicker Anzeige
            redirect: null,
        };
    }

    async componentDidMount() {
        const currentUser = AuthService.getCurrentUser();
        if (!currentUser) this.setState({redirect: "/sign-in"});

        const schwerpunktRequest = axios.get(`/api/schwerpunkt`); //alle Schwerpunkte für RadioButtons
        const disziplinRequest = axios.get(`/api/disziplin`); //alle Disziplinen für Checkboxen
        const trainDiszRequest = axios.get(`/api/training/disziplin/${this.state.selectedTraining}`);


        await axios
            .all([schwerpunktRequest, disziplinRequest, trainDiszRequest])
            .then(axios.spread((...res) => {
                const resSchwerpunkt = res[0]
                const resDisziplin = res[1]
                const resTrainDisz = res[2]

                this.setState({schwerpunkt: resSchwerpunkt.data})
                this.setState({disziplin: resDisziplin.data})
                this.setState({trainDisz: resTrainDisz.data})

            }))
            .catch(errors => {
                console.log(errors)
            });
        this.loadDiszForSchwerpunkt(this.state.selectedSchwerpunkt);
    }

    loadDiszForSchwerpunkt = (id) => {
        this.setState({disziplinSchwerpunkt: []});

        for (let i = 0; i < this.state.disziplin.length; i++) {
            let diszObject = this.state.disziplin[i];

            if (`${this.state.disziplin[i].schwerpunktId}` === id) {
                this.setState(prevState => ({
                    disziplinSchwerpunkt: [...prevState.disziplinSchwerpunkt, diszObject]
                }))
            }
        }
        this.setState({selectedDisziplin: `${this.state.disziplinSchwerpunkt[0].disziplinId}`});
    }

    relodDisziplin = (training) => {
        const trainingRequest = axios.get(`/api/training/${training}`);
        const trainDiszRequest = axios.get(`/api/training/disziplin/${training}`);

        axios
            .all([trainingRequest, trainDiszRequest])
            .then(axios.spread((...res) => {
                const resTraining = res[0]
                const resTrainDisz = res[1]

                this.setState({training: resTraining.data})
                this.setState({trainDisz: resTrainDisz.data})

            }))
            .catch(errors => {
                console.log(errors)
            });
    }


    handleSchwerpunktChange = event => {
        this.setState({selectedSchwerpunkt: event.target.value})
        this.loadDiszForSchwerpunkt(event.target.value)
    };

    handleDiszChange = (event) => {
        this.setState({selectedDisziplin: event.target.value})
    };

    handleTrainingChange = (event) => {                             //kann in app component genommen werden
        this.setState({trainDisz: []});
        this.setState({selectedTraining: event.target.value});

        axios
            .get(`/api/training/disziplin/${event.target.value}`)
            .then(res => {
                this.setState({trainDisz: res.data})
            })
            .catch(err => {
                console.error(err);
            });
    };

    handleSubmit = (event) => {
        event.preventDefault();

        const requestData = {
            trainingId: parseInt(this.state.selectedTraining),
            disziplinId: parseInt(this.state.selectedDisziplin)
        };
        axios
            .post("/api/training/disziplin", requestData)
            .then(res => {
                this.relodDisziplin(this.state.selectedTraining);
            })
            .catch(err => {
                console.log(err);
            });
    };

    handleRemoveDisz = (event) => {
        event.preventDefault();

        const requestData = {
            trainingId: parseInt(this.state.selectedTraining),
            disziplinId: parseInt(event.currentTarget.attributes[0].nodeValue)
        };
        axios
            .delete("/api/disziplin", {data: requestData})
            .then(res => {
                this.relodDisziplin(this.state.selectedTraining);
            })
            .catch(err => {
                console.log(err);
            });
    }


    //---------rendering------------
    render() {
        if (this.state.redirect) {
            return <Redirect to={this.state.redirect}/>
        }
        return (
            <div id="grobPlanungContainer">
                <form onSubmit={this.handleSubmit}>
                    <h3>Grob Planung</h3>
                    <p><strong><u>Trainingstage</u></strong></p>
                    <div className="form-group calendar">
                        <DatepickerComponent handleDateChange={this.handleDateChange}
                                             selectedTrainingDays={this.props.selectedTrainingDays}/>
                    </div>

                    <TrainingList trainings={this.props.trainings} handleTrainingChange={this.handleTrainingChange}
                                  trainDisz={this.state.trainDisz} handleRemoveDisz={this.handleRemoveDisz}
                                  selectedTraining={this.state.selectedTraining}/>

                    {this.state.selectedTraining === "0" ?
                        (
                            <div></div>
                        ) : (
                            <div>
                                <p><strong><u>Disizplin zum Training hinzufügen:</u></strong></p>
                                <Form.Group>
                                    <SchwerpunktListe schwerpunkt={this.state.schwerpunkt}
                                                      selectedSchwerpunkt={this.state.selectedSchwerpunkt}
                                                      handleSchwerpunktChange={this.handleSchwerpunktChange}/>
                                    <DisziplinenListe disziplinSchwerpunkt={this.state.disziplinSchwerpunkt}
                                                      handleDiszChange={this.handleDiszChange}/>
                                </Form.Group>

                                <Button className="submitBtn" type="submit">Training aktualisieren</Button>
                            </div>
                        )}
                </form>
                <span className="addTrainingBtn">
                            {
                                /**
                                 * Some buttons don't contain text, which makes them not accessible
                                 * As described in a short thread (Making accessible icon buttons by Nicholas C. Zakas)
                                 * when the "alt" attribute is used on the content or the "title" attribute is used on
                                 * the button, the buttons get announced differently or not at all
                                 * Thus we used "aria-label" on the buttons that don't contain text, and no attribute
                                 * on the content, the support should hopefully be given with almost all screen-readers
                                 * and browsers by now
                                 *
                                 * For the function see {@link App.addTraining}
                                 */
                            }
                    <button aria-label="Training hinzufügen" onClick={this.props.handleModalShowHide}>
                                <PlusCircleFill color="darkorange" size="2.5rem"/>
                            </button>
                </span>
                <AddTraining showHide={this.props.showHide} handleModalShowHide={this.props.handleModalShowHide}
                             handleDateChange={this.props.handleDateChange} addTraining={this.props.addTraining}/>
            </div>
        );
    }
}
