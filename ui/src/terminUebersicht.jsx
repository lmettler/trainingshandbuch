import React, {Component} from "react";
import AuthService from "./services/auth.service";
import {Redirect} from "react-router-dom";

export default class TerminUebersicht extends Component {

    constructor(props) {
        super(props);

        this.state = {
            redirect: null,
        };
    }

    componentDidMount() {
        const currentUser = AuthService.getCurrentUser();
        if (!currentUser) this.setState({redirect: "/sign-in"});
    }

    render() {

        if (this.state.redirect) {
            return <Redirect to={this.state.redirect}/>
        }
        return (
            <form>
                <h3>Termin Übersicht</h3>
                <div className="form-group">
                    Calendar is on the way
                </div>
                <div className="form-group">
                </div>
            </form>
        );
    }
}
