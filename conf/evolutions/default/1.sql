# Handbuch schema
# Schwerpunkt schema
# tUser schema
# Disziplin schema
# Handbuch schema

# --- !Ups
create table handbuch(
handbuchId integer GENERATED ALWAYS AS IDENTITY,
gruppenname varchar(50) NOT NULL,
startdatum date NOT NULL,
enddatum date NOT NULL,
trainingstag varchar(50) NOT NULL,
turnverein varchar(50) NOT NULL,
PRIMARY KEY (handbuchId)
);

create table tUser(
userId integer GENERATED ALWAYS AS IDENTITY,
vorname varchar(50) NOT NULL,
nachname varchar(50) NOT NULL,
passwort varchar(200) NOT NULL,
email varchar(50) NOT NULL,
turnverein varchar(50) NOT NULL,
PRIMARY KEY (userId)
);

create table schwerpunkt(
schwerpunktId integer GENERATED ALWAYS AS IDENTITY,
schwerpunktName varchar(50) NOT NULL,
PRIMARY KEY (schwerpunktId)
);

create table disziplin(
disziplinId integer GENERATED ALWAYS AS IDENTITY,
disziplinName varchar(50) NOT NULL,
schwerpunktId integer NOT NULL,
CONSTRAINT fk_Schwerpunkt
    FOREIGN KEY(schwerpunktId)
        REFERENCES schwerpunkt(schwerpunktId),
PRIMARY KEY (disziplinId)
);

create table training(
trainingId integer GENERATED ALWAYS AS IDENTITY,
handbuchId integer NOT NULL,
trainingDatum date NOT NULL,
trainingArt varchar(50),
einleitung text,
hauptteil text,
schluss text,
bemerkungen text,
CONSTRAINT fk_Handbuch
    FOREIGN KEY(handbuchId)
        REFERENCES handbuch(handbuchId),
PRIMARY KEY (trainingId)
);

create table disziplin_training(
trainingId integer,
disziplinId integer,
CONSTRAINT fk_Training
    FOREIGN KEY(trainingId)
        REFERENCES training(trainingId),
CONSTRAINT fk_Disziplin
    FOREIGN KEY(disziplinId)
        REFERENCES disziplin(disziplinId)
);

create table handbuch_user(
handbuch_userId integer GENERATED ALWAYS AS IDENTITY,
userId integer,
handbuchId integer,
CONSTRAINT fk_Handbuch
    FOREIGN KEY(handbuchId)
        REFERENCES handbuch(handbuchId),
CONSTRAINT fk_User
    FOREIGN KEY(userId)
        REFERENCES tUser(userId)
);


# --- !Downs
drop table handbuch;
drop table tUser;
drop table schwerpunkt;
drop table disziplin;
drop table training;
drop table disziplin_training;
drop table handbuch_user;
