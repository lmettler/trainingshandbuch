# --- !Ups
ALTER table disziplin_training ADD COLUMN disziplin_trainingId integer GENERATED ALWAYS AS IDENTITY;
# --- !Downs
ALTER table disziplin_training DROP COLUMN disziplin_trainingId;